import mysql.connector

import xml.etree.ElementTree as ET

mydb = mysql.connector.connect(
    host="localhost", user="username", password="password", database="database_name"
)

mycursor = mydb.cursor()


file_path = "MemberData.xml"


def make_sql_fields(subcommittee):
    obj = {}
    for i in subcommittee.keys():
        newkey = i.replace("-", "_")
        obj[newkey] = subcommittee[i]
    return obj


def remove_None(dictionary):
    obj = {}
    for i in dictionary.keys():
        if dictionary[i]:
            obj[i] = dictionary[i]
    return obj


def insert_data(subcommittee, table):

    dictionary = make_sql_fields(subcommittee)
    fields = str(list(dictionary.keys()))[1:-1]
    values = str(list(dictionary.values()))[1:-1]
    sql = "INSERT INTO " + table + " (" + fields + ")"
    sql = sql.replace("'", "") + " VALUES (" + values + ")"
    print(sql)
    try:
        mycursor.execute(sql)
        mydb.commit()
    except Exception as e:
        print(e)


tree = ET.parse(file_path)
root = tree.getroot()
committees = root[2]
for committee in committees.findall("committee"):
    for fn in committee.findall("committee-fullname"):
        committee.attrib["committee-fullname"] = fn.text
    for ratio in committee.findall("ratio"):
        for majority in ratio.findall("majority"):
            committee.attrib["majority"] = majority.text
        for minority in ratio.findall("minority"):
            committee.attrib["minority"] = minority.text
    committee.attrib["com_type"] = committee.attrib["type"]
    del committee.attrib["type"]
    insert_data(committee.attrib, "committee")
    ###GET ALL SUB
    for subcommittee in committee.findall("subcommittee"):
        subcommittee.attrib["comcode"] = committee.attrib["comcode"]
        for ratio in subcommittee.findall("ratio"):
            for majority in ratio.findall("majority"):
                subcommittee.attrib["majority"] = majority.text
            for minority in ratio.findall("minority"):
                subcommittee.attrib["minority"] = minority.text
        insert_data(subcommittee.attrib, "subcommittee")


members = root[1]
for member in members.findall("member"):
    member_obj = {}
    for statedistrict in member.findall("statedistrict"):
        member_obj["statedistrict"] = statedistrict.text
    for info in member.findall("member-info"):
        for child in info:
            if child.attrib:
                member_obj[child.tag] = child.attrib[child.keys()[0]]

            else:
                member_obj[child.tag] = child.text
    if "bioguideID" in member_obj.keys() and member_obj["bioguideID"]:
        member_obj = remove_None(member_obj)
        insert_data(member_obj, "member")

    for coms in member.findall("committee-assignments"):
        for child in coms:
            child.attrib["bioguideID"] = member_obj["bioguideID"]
            if "comcode" in child.attrib.keys():
                insert_data(child.attrib, "member_committee")
            if "subcomcode" in child.attrib.keys():
                insert_data(child.attrib, "member_subcommittee")
