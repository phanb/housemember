from graphene import ObjectType, Schema, String


from app.modules.subcommittee.queries import SubcommitteeQuery
from app.modules.subcommittee.mutations import SubcommitteeMutation

from app.modules.committee.queries import CommitteeQuery
from app.modules.committee.mutations import CommitteeMutation

from app.modules.member.queries import MemberQuery
from app.modules.member.mutations import MemberMutation

from app.modules.memberCommittee.queries import MemberCommitteeQuery
from app.modules.memberCommittee.mutations import MemberCommitteeMutation

from app.modules.memberSubcommittee.queries import MemberSubcommitteeQuery
from app.modules.memberSubcommittee.mutations import MemberSubcommitteeMutation


class RootQuery(
    SubcommitteeQuery,
    CommitteeQuery,
    MemberQuery,
    MemberCommitteeQuery,
    MemberSubcommitteeQuery,
    ObjectType,
):
    pass


class RootMutation(
    SubcommitteeMutation,
    CommitteeMutation,
    MemberMutation,
    MemberCommitteeMutation,
    MemberSubcommitteeMutation,
    ObjectType,
):
    pass


schema = Schema(query=RootQuery, mutation=RootMutation)
