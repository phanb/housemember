from flask import current_app as app
from app import db
from app.models.MemberCommittee import MemberCommittee
from app.models.MemberSubcommittee import MemberSubcommittee
from app.models.Committee import Committee
from app.models.Subcommittee import Subcommittee


class Member(db.Model):
    __tablename__ = "member"

    bioguideID = db.Column(db.String(100), primary_key=True, nullable=False)
    namelist = db.Column(db.String(25), nullable=True, default="")
    lastname = db.Column(db.String(25), nullable=True, default="")
    firstname = db.Column(db.String(25), nullable=True)
    middlename = db.Column(db.String(25), nullable=True)
    sort_name = db.Column(db.String(25), nullable=True)
    suffix = db.Column(db.String(25), nullable=True)
    courtesy = db.Column(db.String(10), nullable=True)
    prior_congress = db.Column(db.Integer, nullable=True)
    official_name = db.Column(db.String(100), nullable=True)
    formal_name = db.Column(db.String(100), nullable=True)
    party = db.Column(db.String(25), nullable=True)
    caucus = db.Column(db.String(25), nullable=True)
    state = db.Column(db.String(25), nullable=True)
    district = db.Column(db.String(25), nullable=True)
    townname = db.Column(db.String(25), nullable=True)
    office_building = db.Column(db.String(25), nullable=True)
    office_room = db.Column(db.String(25), nullable=True)
    office_zip = db.Column(db.String(25), nullable=True)
    office_zip_suffix = db.Column(db.String(25), nullable=True)
    phone = db.Column(db.String(25), nullable=True)
    elected_date = db.Column(db.String(25), nullable=True)
    sworn_date = db.Column(db.String(25), nullable=True)
    statedistrict = db.Column(db.String(25), nullable=True)
    footnote_ref = db.Column(db.String(25), nullable=True)
    footnote = db.Column(db.Text(), nullable=True)

    committees = db.relationship(
        "MemberCommittee", uselist=True, back_populates="member"
    )
    subcommittees = db.relationship(
        "MemberSubcommittee", uselist=True, back_populates="member"
    )

    def __init__(
        self,
        bioguideID,
        namelist,
        lastname,
        firstname,
        middlename,
        sort_name,
        suffix,
        courtesy,
        prior_congress,
        official_name,
        formal_name,
        party,
        caucus,
        state,
        district,
        townname,
        office_building,
        office_room,
        office_zip,
        office_zip_suffix,
        phone,
        elected_date,
        sworn_date,
    ):
        self.bioguideID = bioguideID
        self.namelist = namelist
        self.lastname = lastname
        self.firstname = firstname
        self.middlename = middlename
        self.sort_name = sort_name
        self.suffix = suffix
        self.courtesy = courtesy
        self.prior_congress = prior_congress
        self.official_name = official_name
        self.formal_name = formal_name
        self.party = party
        self.caucus = caucus
        self.state = state
        self.district = district
        self.townname = townname
        self.office_building = office_building
        self.office_room = office_room
        self.office_zip = office_zip
        self.office_zip_suffix = office_zip_suffix
        self.phone = phone
        self.elected_date = elected_date
        self.sworn_date = sworn_date

    def save(self):
        db.session.add(self)
        db.session.commit()
