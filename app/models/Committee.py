from flask import current_app as app
from app import db
from sqlalchemy.orm import relationship

# from app.models.Subcommittee import Subcommittee


class Committee(db.Model):
    __tablename__ = "committee"

    comcode = db.Column(db.String(100), primary_key=True, nullable=False)
    com_building_code = db.Column(db.String(25), nullable=True)
    com_header_text = db.Column(db.Text(), nullable=True)
    com_phone = db.Column(db.String(25), nullable=True)
    com_room = db.Column(db.String(25), nullable=True)
    com_zip = db.Column(db.String(25), nullable=True)
    com_zip_suffix = db.Column(db.String(25), nullable=True)
    com_type = db.Column(db.String(25), nullable=True)
    majority = db.Column(db.String(25), nullable=True)
    minority = db.Column(db.String(25), nullable=True)
    committee_fullname = db.Column(db.String(100), nullable=True)
    subcommittee = db.relationship(
        "Subcommittee", uselist=True, back_populates="committee"
    )

    def __init__(
        self,
        comcode,
        com_building_code,
        com_header_text,
        com_phone,
        com_room,
        com_zip,
        com_zip_suffix,
        com_type,
        majority,
        minority,
    ):
        self.comcode = comcode
        self.com_building_code = com_building_code
        self.com_header_text = com_header_text
        self.com_phone = com_phone
        self.com_room = com_room
        self.com_zip = com_zip
        self.com_zip_suffix = com_zip_suffix
        self.com_type = com_type
        self.majority = majority
        self.minority = minority

    def save(self):
        db.session.add(self)
        db.session.commit()
