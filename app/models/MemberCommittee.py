from flask import current_app as app
from app import db
from sqlalchemy.orm import relationship


class MemberCommittee(db.Model):
    __tablename__ = "member_committee"

    comcode = db.Column(
        db.String(100), db.ForeignKey("committee.comcode"), primary_key=True
    )
    bioguideID = db.Column(
        db.String(100), db.ForeignKey("member.bioguideID"), primary_key=True
    )
    leadership = db.Column(db.String(100), nullable=True)
    rank = db.Column(db.Integer, nullable=False)
    committee = relationship("Committee")
    member = relationship("Member")

    def __init__(self, comcode, bioguideID, rank, leadership):
        self.comcode = comcode
        self.bioguideID = bioguideID
        self.rank = rank
        self.leadership = leadership

    def save(self):
        db.session.add(self)
        db.session.commit()
