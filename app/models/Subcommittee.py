from flask import current_app as app
from app import db
from sqlalchemy.orm import relationship
from app.models.Committee import Committee


class Subcommittee(db.Model):
    __tablename__ = "subcommittee"

    subcomcode = db.Column(db.String(100), primary_key=True, nullable=False)
    comcode = db.Column(db.String(100), db.ForeignKey("committee.comcode"))
    subcommittee_fullname = db.Column(db.String(25), nullable=True)
    subcom_room = db.Column(db.String(25), nullable=True)
    subcom_zip = db.Column(db.String(25), nullable=True)
    subcom_zip_suffix = db.Column(db.String(25), nullable=True)
    subcom_building_code = db.Column(db.String(25), nullable=True)
    subcom_phone = db.Column(db.String(25), nullable=True)
    majority = db.Column(db.String(25), nullable=True)
    minority = db.Column(db.String(25), nullable=True)
    committee = relationship("Committee", back_populates="subcommittee")

    def __init__(
        self,
        subcomcode,
        comcode,
        subcommittee_fullname,
        subcom_room,
        subcom_zip,
        subcom_zip_suffix,
        subcom_building_code,
        subcom_phone,
        majority,
        minority,
    ):

        self.subcomcode = subcomcode
        self.comcode = comcode
        self.subcommittee_fullname = subcommittee_fullname
        self.subcom_room = subcom_room
        self.subcom_zip = subcom_zip
        self.subcom_zip_suffix = subcom_zip_suffix
        self.subcom_building_code = subcom_building_code
        self.subcom_phone = subcom_phone
        self.majority = majority
        self.minority = minority

    def save(self):
        db.session.add(self)
        db.session.commit()
