from flask import current_app as app
from app import db
from sqlalchemy.orm import relationship


class MemberSubcommittee(db.Model):
    __tablename__ = "member_subcommittee"

    subcomcode = db.Column(
        db.String(100), db.ForeignKey("subcommittee.subcomcode"), primary_key=True
    )
    bioguideID = db.Column(
        db.String(100), db.ForeignKey("member.bioguideID"), primary_key=True
    )
    leadership = db.Column(db.String(100), nullable=True)
    rank = db.Column(db.Integer, nullable=False)
    subcommittee = relationship("Subcommittee")
    member = relationship("Member")

    def __init__(self, subcomcode, bioguideID, rank, leadership):
        self.subcomcode = subcomcode
        self.bioguideID = bioguideID
        self.leadership = leadership
        self.rank = rank

    def save(self):
        db.session.add(self)
        db.session.commit()
