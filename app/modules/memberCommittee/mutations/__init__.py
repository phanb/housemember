from graphene import AbstractType

from .AddMemberCommittee import AddMemberCommittee


class MemberCommitteeMutation(AbstractType):
    add_member_committee = AddMemberCommittee.Field()
