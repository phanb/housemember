from graphene import Mutation, Boolean, String, Int
from app.models.MemberCommittee import MemberCommittee as MemberCommitteeModel
from graphql import GraphQLError


class AddMemberCommittee(Mutation):
    ok = Boolean(description="Add Member Record")
    message = String(description="Request message")

    class Input:

        comcode = String(description="Self Descriptive")
        bioguideID = String(description="Self Descriptive")
        rank = String(description="Self Descriptive")
        leadership = String(description="Self Descriptive")

    def mutate(self, info, **kwargs):
        member_committee = MemberCommitteeModel(**kwargs)
        try:
            member_committee.save()

        except Exception as e:
            raise GraphQLError("Error creating MemberCommittee object.", e)
        else:
            ok = True
            message = "member have been created successfully"
            return AddMemberCommittee(ok=ok, message=message)
