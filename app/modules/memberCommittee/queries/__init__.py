from graphene import ObjectType

from .MemberCommittee import MemberCommittee


class MemberCommitteeQuery(MemberCommittee, ObjectType):
    pass
