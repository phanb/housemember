from graphene import ObjectType, Field, String, List, relay, InputObjectType
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models.MemberCommittee import MemberCommittee as MemberCommitteeModel
from app.models.Committee import Committee as CommitteeModel
from app.modules.committee.queries.Committee import CommitteeType
from graphql import GraphQLError


class ConnectionConfig(InputObjectType):
    comcode = String(required=True)


class MemberCommitteeType(SQLAlchemyObjectType):
    class Meta:
        model = MemberCommitteeModel
        interfaces = (relay.Node,)

    committee = List(CommitteeType)

    def resolve_committee(self, info, **args):
        committees = CommitteeModel.query.filter_by(comcode=self.comcode)
        if committees:
            return committees
        return []


class MemberCommittee(ObjectType):
    member_committee_by_comcode = Field(
        List(MemberCommitteeType),
        comcode=String(),
        description="""
        get member belogn to committee
        """,
    )
    member_committee = Field(
        List(MemberCommitteeType),
        description="""
    :This is MemberCommitteeModel.

    :description: Returns the many member many committee.
    
    """,
    )

    def resolve_member_committee_by_comcode(self, info, **kwargs):
        comcode = kwargs.get("comcode", "")
        member_committee = MemberCommitteeModel.query.filter_by(comcode=comcode)
        if member_committee:
            return member_committee
        else:
            raise GraphQLError("member_committee is not found.")

    def resolve_member_committee(self, info, **args):
        member_committee = MemberCommitteeModel.query.filter_by()
        if member_committee:
            return member_committee
        else:
            raise GraphQLError("member_committee is not found.")
