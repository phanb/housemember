from graphene import AbstractType

from .AddSubcommittee import AddSubcommittee


class SubcommitteeMutation(AbstractType):
    add_subcommittee = AddSubcommittee.Field()
