from graphene import Mutation, Boolean, String
from app.models.Subcommittee import Subcommittee as SubcommitteeModel
from graphql import GraphQLError


class AddSubcommittee(Mutation):
    ok = Boolean(description="Request status")
    message = String(description="Request message")

    class Input:
        subcomcode = String(description="Self Descriptive")
        comcode = String(description="Self Descriptive")
        subcommittee_fullname = String(description="Self Descriptive")
        subcom_room = String(description="Self Descriptive")
        subcom_zip = String(description="Self Descriptive")
        subcom_zip_suffix = String(description="Self Descriptive")
        subcom_building_code = String(description="Self Descriptive")
        subcom_phone = String(description="Self Descriptive")
        majority = String(description="Self Descriptive")
        minority = String(description="Self Descriptive")

    def mutate(self, info, **kwargs):
        subcommittee = SubcommitteeModel(**kwargs)
        try:
            subcommittee.save()

        except Exception as e:
            raise GraphQLError("Error creating Subcommittee object.", e)
        else:
            ok = True
            message = "Subcommittee have been created successfully"
            return AddSubcommittee(ok=ok, message=message)
