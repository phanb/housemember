from graphene import ObjectType, Field, String, List, relay
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models.Subcommittee import Subcommittee as SubcommitteeModel
from graphql import GraphQLError


class SubcommitteeType(SQLAlchemyObjectType):
    class Meta:
        model = SubcommitteeModel
        interfaces = (relay.Node,)


class Subcommittee(ObjectType):
    subcommittees = Field(
        List(SubcommitteeType),
        description="""
    :get all subcommitte
    :description: Returns all subcommitte Object 
    """,
    )

    def resolve_subcommittees(self, info):
        subcommiitte = SubcommitteeModel.query.filter_by()
        if subcommiitte:
            return subcommiitte
        else:
            raise GraphQLError("subcommittee is empty")
