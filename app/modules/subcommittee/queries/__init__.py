from graphene import ObjectType

from .Subcommittee import Subcommittee


class SubcommitteeQuery(Subcommittee, ObjectType):
    pass
