from graphene import Mutation, Boolean, String, Int
from app.models.Member import Member as MemberModel
from graphql import GraphQLError


class AddMember(Mutation):
    ok = Boolean(description="Add Member Record")
    message = String(description="Request message")

    class Input:
        bioguideID = String(description="Self Descriptive")
        namelist = String(description="Self Descriptive")
        lastname = String(description="Self Descriptive")
        firstname = String(description="Self Descriptive")
        middlename = String(description="Self Descriptive")
        sort_name = String(description="Self Descriptive")
        suffix = String(description="Self Descriptive")
        courtesy = String(description="Self Descriptive")
        prior_congress = String(description="Self Descriptive")
        official_name = String(description="Self Descriptive")
        formal_name = String(description="Self Descriptive")
        party = String(description="Self Descriptive")
        caucus = String(description="Self Descriptive")
        state = String(description="Self Descriptive")
        district = String(description="Self Descriptive")
        townname = String(description="Self Descriptive")
        office_building = String(description="Self Descriptive")
        office_room = String(description="Self Descriptive")
        office_zip = String(description="Self Descriptive")
        office_zip_suffix = String(description="Self Descriptive")
        phone = String(description="Self Descriptive")
        elected_date = String(description="Self Descriptive")
        sworn_date = String(description="Self Descriptive")

    def mutate(self, info, **kwargs):
        member = MemberModel(**kwargs)
        try:
            member.save()

        except Exception as e:
            raise GraphQLError("Error creating Member object.", e)
        else:
            ok = True
            message = "member have been created successfully"
            return AddMember(ok=ok, message=message)
