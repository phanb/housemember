from graphene import AbstractType

from .AddMember import AddMember


class MemberMutation(AbstractType):
    add_member = AddMember.Field()
