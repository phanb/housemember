from graphene import ObjectType, Field, String, List, relay, InputObjectType
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models.Member import Member as MemberModel
from app.models.MemberCommittee import MemberCommittee as MemberCommitteeModel
from app.models.Committee import Committee as CommitteeModel
from app.modules.memberCommittee.queries.MemberCommittee import MemberCommitteeType
from app.modules.memberSubcommittee.queries.MemberSubcommittee import (
    MemberSubcommitteeType,
)

from graphql import GraphQLError


class ConnectionConfig(InputObjectType):
    bioguideID = String(required=True)


class MemberType(SQLAlchemyObjectType):
    class Meta:
        model = MemberModel

    committees = List(MemberCommitteeType)
    subcommittees = List(MemberSubcommitteeType)


class Member(ObjectType):

    member_by_bioguideID = Field(
        MemberType,
        bioguideID=String(),
        description="""
    Find member by id
    Returns a members object
    """,
    )

    members = Field(
        List(MemberType),
        description="""
    Get all member
    Returns all member.
    """,
    )

    def resolve_members(self, info):
        members = MemberModel.query.filter_by()
        if members:
            return members
        else:
            raise GraphQLError("members is not found.")

    def resolve_member_by_bioguideID(self, info, **kwargs):
        bioguideID = kwargs.get("bioguideID", "")
        member = MemberModel.query.filter_by(bioguideID=bioguideID).scalar()
        if member:
            return member
        else:
            raise GraphQLError("members is not found.")
        return member
