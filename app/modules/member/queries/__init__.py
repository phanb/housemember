from graphene import ObjectType

from .Member import Member


class MemberQuery(Member, ObjectType):
    pass
