from graphene import ObjectType, Field, String, List, relay
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models.MemberSubcommittee import MemberSubcommittee as MemberSubcommitteeModel
from graphql import GraphQLError


class MemberSubcommitteeType(SQLAlchemyObjectType):
    class Meta:
        model = MemberSubcommitteeModel
        interfaces = (relay.Node,)


class MemberSubcommittee(ObjectType):
    member_subcommittee = Field(
        List(MemberSubcommitteeType),
        description="""
    :This is MemberSubcommitteeModel.

    :description: Returns all MemberSubcommittee
    """,
    )

    def resolve_member(self, info, **args):
        member_subcommittee = MemberSubcommitteeModel.query.filter_by()

        if member_subcommittee:
            return member_subcommittee
        else:
            raise GraphQLError("member_subcommittee is not found.")
