from graphene import ObjectType
from .MemberSubcommittee import MemberSubcommittee


class MemberSubcommitteeQuery(MemberSubcommittee, ObjectType):
    pass
