from graphene import AbstractType

from .AddMemberSubcommittee import AddMemberSubcommittee


class MemberSubcommitteeMutation(AbstractType):
    add_member_subcommittee = AddMemberSubcommittee.Field()
