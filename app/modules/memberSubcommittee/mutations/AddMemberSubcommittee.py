from graphene import Mutation, Boolean, String, Int
from app.models.MemberSubcommittee import MemberSubcommittee as MemberSubcommitteeModel
from graphql import GraphQLError


class AddMemberSubcommittee(Mutation):
    ok = Boolean(description="Add Member Record")
    message = String(description="Request message")

    class Input:

        subcomcode = String(description="Self Descriptive")
        bioguideID = String(description="Self Descriptive")
        rank = String(description="Self Descriptive")
        leadership = String(description="Self Descriptive")

    def mutate(self, info, **kwargs):
        member_subcommittee = MemberSubcommitteeModel(**kwargs)
        try:
            member_subcommittee.save()

        except Exception as e:
            raise GraphQLError("Error creating MemberSubcommittee object.", e)
        else:
            ok = True
            message = "member have been created successfully"
            return AddMemberSubcommittee(ok=ok, message=message)
