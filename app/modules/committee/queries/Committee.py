from graphene import ObjectType, Field, String, List, relay
from graphene_sqlalchemy import SQLAlchemyObjectType
from app.models.Committee import Committee as CommitteeModel
from app.models.Subcommittee import Subcommittee as SubcommitteeModel
from graphql import GraphQLError


class CommitteeType(SQLAlchemyObjectType):
    class Meta:
        model = CommitteeModel
        interfaces = (relay.Node,)


class Committee(ObjectType):
    committees = Field(
        List(CommitteeType),
        description="""
    :getall committees
    :description: Returns all committees
    """,
    )

    def resolve_committees(self, info, **args):
        committee = CommitteeModel.query.filter_by()

        if committee:
            return committee
        else:
            raise GraphQLError("Commiitte is not found.")
