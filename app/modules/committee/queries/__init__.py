from graphene import ObjectType

from .Committee import Committee


class CommitteeQuery(Committee, ObjectType):
    pass
