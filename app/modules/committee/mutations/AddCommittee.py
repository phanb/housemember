from graphene import Mutation, Boolean, String
from app.models.Committee import Committee as CommitteeModel
from graphql import GraphQLError


class AddCommittee(Mutation):
    ok = Boolean(description="Add Committee Record")
    message = String(description="Request message")

    class Input:
        comcode = String(description="Self Descriptive")
        com_building_code = String(description="Self Descriptive")
        com_header_text = String(description="Self Descriptive")
        com_phone = String(description="Self Descriptive")
        com_room = String(description="Self Descriptive")
        com_zip = String(description="Self Descriptive")
        com_zip_suffix = String(description="Self Descriptive")
        com_type = String(description="Self Descriptive")
        majority = String(description="Self Descriptive")
        minority = String(description="Self Descriptive")
        committee_fullname = String(description="Self Descriptive")

    def mutate(self, info, **kwargs):
        committee = CommitteeModel(**kwargs)
        try:
            committee.save()

        except Exception as e:
            raise GraphQLError("Error creating Committee object.", e)
        else:
            ok = True
            message = "Committee have been created successfully"
            return AddCommittee(ok=ok, message=message)
