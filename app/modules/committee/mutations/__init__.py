from graphene import AbstractType

from .AddCommittee import AddCommittee


class CommitteeMutation(AbstractType):
    add_committee = AddCommittee.Field()
