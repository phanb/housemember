## Project Description:
    This project create API from https://clerk.house.gov/xml/lists/MemberData.xml 
    Project stack 
        python-Flask
        mysql
        graphql
*Do note that it's never a good idea to commit your database user and password credentials in the repository, use a `.env` file instead. This repository is just for reference purpose.*
    
## Project Setup

**Ensure that `docker v1.3` and above is installed**
```
$ git clone https://gitlab.com/phanb/housemember.git
$ cd housemember
$ docker-compose up
```
On your browser go to http://<dev_ip>:5000/graphql

### Set up database:
1) From mysql file.<br> 
Open a new terminal then execute:
```sh
docker exec -it database bash  docker-entrypoint-initdb.d/restore_db.sh
```
2) Or execute `parse_data.py` (This process is slower! The script parsing the xml file and insert record to mysql):
```
    python3 python3 parse_data.py
```

### Models:
![image-1.png](./image-1.png)

## Add record with graphql:
### Add a member:
```sh
mutation{addMember(
bioguideID:"AAAAA"
caucus: "R"
courtesy:"R"
district: "test"
electedDate: "20201103"
firstname: "firstname"
formalName: "formalname"
lastname: "lastname"
middlename: "mid"
namelist: "a name list"
officeBuilding: "a building"
officeRoom: "111"
officeZip: "1111"
officeZipSuffix: "01234"
officialName: "official name"
party: "R"
phone: "111-111-1111"
priorCongress: "113"
sortName: "my sort name"
state: "va"
suffix: "mr"
swornDate: "20210103"
townname: "SP"
) {
  ok
  message
}
}
```
return: 
```sh
{
  "data": {
    "addMember": {
      "ok": true,
      "message": "member have been created successfully"
    }
  }
}
```
### Add a committee:
```sh
mutation{
  addCommittee(
        comcode: "aaaa",
        comBuildingCode: "LHOB",
        comHeaderText: "Test add committee",
        comPhone: "225-2171",
        comRoom: "1301",
        comZip: "20515",
        comZipSuffix: "6001",
        comType: "standing",
        majority: "27",
        minority: "24",    
    
  ) {
    ok
    message
  }
  
}
```
Return:
```sh
{
  "data": {
    "addCommittee": {
      "ok": true,
      "message": "Committee have been created successfully"
    }
  }
}
```


#### Add a committee to a member:
```sh
mutation{
  addMemberCommittee(comcode:"aaaa",bioguideID:"A000148", rank:"1", leadership:"") {
    ok
    message
  }
}
```
Return:

```sh
{
  "data": {
    "addMemberCommittee": {
      "ok": true,
      "message": "member have been created successfully"
    }
  }
}
```

## Query data with graphql
#### Query all members:
```
query{
members {
  bioguideID  namelist  lastname   firstname  middlename  sortName  suffix  courtesy  priorCongress officialName formalName party caucus state district townname officeBuilding officeRoom officeZip officeZipSuffix phone electedDate swornDate statedistrict footnoteRef footnote
}
}
```

Return:

```
{
  "data": {
    "members": [
      {
        "bioguideID": "A000055", "namelist": "Aderholt, Robert", "lastname": "Aderholt", "firstname": "Robert", "middlename": "B.", "sortName": "ADERHOLT,ROBERT", "suffix": null, "courtesy": "Mr.", "priorCongress": 116, "officialName": "Robert B. Aderholt", "formalName": "Mr. Aderholt", "party": "R", "caucus": "R", "state": "AL", "district": "4th", "townname": "Haleyville", "officeBuilding": "CHOB", "officeRoom": "266", "officeZip": "20515", "officeZipSuffix": "0104", "phone": "(202) 225-4876", "electedDate": "20201103", "swornDate": "20210103", "statedistrict": "AL04", "footnoteRef": null, "footnote": null
      },
    .....
```


#### Query all member by bioguideID:
```sh
query{
memberByBioguideid(bioguideID:"A000148") {
  namelist
  lastname
  firstname
  middlename
  sortName
}
}

```
return 

```sh
{
  "data": {
    "memberByBioguideid": {
      "namelist": "Auchincloss, Jake",
      "lastname": "Auchincloss",
      "firstname": "Jake",
      "middlename": null,
      "sortName": "AUCHINCLOSS,JAKE"
    }
  }
}
```
#### Find all committee and subcommittee info of a member:

```sh
query{
memberByBioguideid(bioguideID:"A000148") {
  bioguideID
  sortName
  committees{
    rank
    comcode
    leadership
    committee{
      committeeFullname
    }
  }
  subcommittees {
    rank
    subcomcode
    leadership
    subcommittee{
      subcomPhone      
    }
  }
}
}
```
Return:

```sh
{
  "data": {
    "memberByBioguideid": {
      "bioguideID": "A000148",
      "sortName": "AUCHINCLOSS,JAKE",
      "committees": [
        {
          "rank": 30,
          "comcode": "BA00",
          "leadership": "Vice Chair",
          "committee": [
            {
              "committeeFullname": "Committee on Financial Services"
            }
          ]
        },
        {
          "rank": 31,
          "comcode": "PW00",
          "leadership": null,
          "committee": [
            {
              "committeeFullname": "Committee on Transportation and Infrastructure"
            }
          ]
        }
      ],
      "subcommittees": [
        {
          "rank": 9,
          "subcomcode": "BA01",
          "leadership": null,
          "subcommittee": {
            "subcomPhone": "225-4247"
          }
        },
        {
          "rank": 8,
          "subcomcode": "BA13",
          "leadership": null,
          "subcommittee": {
            "subcomPhone": "225-4247"
          }
        },
        {
          "rank": 3,
          "subcomcode": "PW07",
          "leadership": "Vice Chair",
          "subcommittee": {
            "subcomPhone": "225-4472"
          }
        },
        {
          "rank": 19,
          "subcomcode": "PW12",
          "leadership": null,
          "subcommittee": {
            "subcomPhone": "225-4472"
          }
        },
        {
          "rank": 16,
          "subcomcode": "PW14",
          "leadership": null,
          "subcommittee": {
            "subcomPhone": "225-4472"
          }
        }
      ]
    }
  }
}
```
#### Find all members associate with a committee:
```sh
query{
memberCommitteeByComcode(comcode:"BA00") {
  member {
    lastname
    firstname

  }
}
}
```

Return:

```sh
{
  "data": {
    "memberCommitteeByComcode": [
      {
        "member": {
          "lastname": "Auchincloss",
          "firstname": "Jake"
        }
      },
      {
        "member": {
          "lastname": "Adams",
          "firstname": "Alma"
        }
      },
      {
        "member": {
          "lastname": "Axne",
          "firstname": "Cynthia"
        }
      },
...
```


## To Do list:
#Database:
1) Update field type to refect the datatype, I mostly use varchar in all fields to get the project up and running quickly.
2) Use join instead of sub select statement to improve return time.
3) I didn't have enough time to look at `predecessor` class. 

#Coding:
1) The `parse_data.py` is slow because it inserts record to mysql 1 by 1, need to change this to bulk insert. 
2) Populate database with docker-compose to speed up set up time.
3) Add more features on the graphql query
    - Query by other fields: name, last name....
    - Add pagination
    - Update record(currently we can only create new record)
    - Add delete record
    - Add orderby<br>
#GUI:
1) Apolo+React GUI
